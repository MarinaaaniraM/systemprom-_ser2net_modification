/*
 *  ser2net - A program for allowing telnet connection to serial ports
 *  Copyright (C) 2001  Corey Minyard <minyard@acm.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef CONTROLLER
#define CONTROLLER

#define VERSION "2.7"

#define INBUF_SIZE 255	/* The size of the maximum input command. */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "telnet.h"


/* Initialize the controller code, return -1 on error. */
int controller_init(char *controller_port);

/* This data structure is kept for each control connection. */
typedef struct controller_info {
    int            tcpfd;		/* When connected, the file
                                           descriptor for the TCP
                                           port used for I/O. */
    struct sockaddr_in remote;		/* The socket address of who
                       is connected to this port. */

    unsigned char inbuf[INBUF_SIZE+1];	/* Buffer to receive command on. */
    int  inbuf_count;			/* The number of bytes currently
                       in the inbuf. */

    char *outbuf;			/* The output buffer, NULL if
                       no output. */
    int  outbufsize;			/* Total size of the memory
                       allocated in outbuf. */
    int  outbuf_pos;			/* The current position in the
                       output buffer. */
    int  outbuf_count;			/* The number of bytes
                       (starting at outbuf_pos)
                       left to transmit. */

    void *monitor_port_id;		/* When port monitoring, this is
                       the id given when the monitoring
                       is started.  It is used to stop
                       monitoring. */

    struct controller_info *next;	/* Used to keep these items in
                       a linked list. */

    /* Data used by the telnet processing. */
    telnet_data_t tn_data;
} controller_info_t;

/* List of current control connections. */
extern controller_info_t *controllers;

/* Send some output to a controller port.  The data field is the data
   to write, the count field is the number of bytes to write. */
void controller_output(struct controller_info *cntlr, char *data, int count);

/* Write some data directly to the controllers output port. */
void controller_write(struct controller_info *cntlr, char *data, int count);

#endif /* CONTROLLER */
