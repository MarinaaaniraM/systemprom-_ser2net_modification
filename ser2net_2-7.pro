TEMPLATE =  app
CONFIG +=   console
CONFIG -=   qt

SOURCES +=  utils.c \
            telnet.c \
            ser2net.c \
            selector.c \
            readconfig.c \
            devcfg.c \
            dataxfer.c \
            controller.c

HEADERS +=  utils.h \
            telnet.h \
            selector.h \
            readconfig.h \
            devcfg.h \
            dataxfer.h \
            controller.h

