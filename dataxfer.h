/*
 *  ser2net - A program for allowing telnet connection to serial ports
 *  Copyright (C) 2001  Corey Minyard <minyard@acm.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef DATAXFER
#define DATAXFER

#include "controller.h"
#include "devcfg.h"
#include "selector.h"

#ifdef USE_UUCP_LOCKING
extern int uucp_locking_enabled;
#endif

// ============================================================================
#define PORT_BUFSIZE	1024


typedef struct port_info
{
    int            enabled;		/* If PORT_DISABLED, the port
                       is disabled and the TCP
                       accept port is not
                       operational.  If PORT_RAW,
                       the port is enabled and
                       will not do any telnet
                       negotiations.  If
                       PORT_RAWLP, the port is enabled
                       only for output without any
                       termios setting - it allows
                       to redirect /dev/lpX devices If
                       PORT_TELNET, the port is
                       enabled and it will do
                       telnet negotiations. */

    int            timeout;		/* The number of seconds to
                       wait without any I/O before
                       we shut the port down. */

    int            timeout_left;	/* The amount of time left (in
                       seconds) before the timeout
                       goes off. */

    sel_timer_t *timer;			/* Used to timeout when the no
                       I/O has been seen for a
                       certain period of time. */


    /* Information about the TCP port. */
    char               *portname;       /* The name given for the port. */
    struct sockaddr_in tcpport;		/* The TCP port to listen on
                       for connections to this
                       terminal device. */
    int            acceptfd;		/* The file descriptor used to
                       accept connections on the
                       TCP port. */
    int            tcpfd;		/* When connected, the file
                                           descriptor for the TCP
                                           port used for I/O. */
    struct sockaddr_in remote;		/* The socket address of who
                       is connected to this port. */
    unsigned int tcp_bytes_received;    /* Number of bytes read from the
                       TCP port. */
    unsigned int tcp_bytes_sent;        /* Number of bytes written to the
                       TCP port. */

    /* Information about the terminal device. */
    char           *devname;		/* The full path to the device */
    int            devfd;		/* The file descriptor for the
                                           device, only valid if the
                                           TCP port is open. */
    unsigned int dev_bytes_received;    /* Number of bytes read from the
                       device. */
    unsigned int dev_bytes_sent;        /* Number of bytes written to the
                       device. */


    /* Information use when transferring information from the TCP port
       to the terminal device. */
    int            tcp_to_dev_state;		/* State of transferring
                           data from the TCP port
                                                   to the device. */
    unsigned char  tcp_to_dev_buf[PORT_BUFSIZE]; /* Buffer used for
                                                    TCP to device
                                                    transfers. */
    int            tcp_to_dev_buf_start;	/* The first byte in
                           the buffer that is
                           ready to send. */
    int            tcp_to_dev_buf_count;	/* The number of bytes
                                                   in the buffer to
                                                   send. */
    struct controller_info *tcp_monitor; /* If non-null, send any input
                        received from the TCP port
                        to this controller port. */


    /* Information use when transferring information from the terminal
       device to the TCP port. */
    int            dev_to_tcp_state;		/* State of transferring
                           data from the device to
                                                   the TCP port. */
    unsigned char  dev_to_tcp_buf[PORT_BUFSIZE]; /* Buffer used for
                                                    device to TCP
                                                    transfers. */
    int            dev_to_tcp_buf_start;	/* The first byte in
                           the buffer that is
                           ready to send. */
    int            dev_to_tcp_buf_count;	/* The number of bytes
                                                   in the buffer to
                                                   send. */


// ============================================================================
    int            lastTimeError;
    int            rdy_signal;
// ============================================================================



    struct controller_info *dev_monitor; /* If non-null, send any input
                        received from the device
                        to this controller port. */

    struct port_info *next;		/* Used to keep a linked list
                       of these. */

    int config_num; /* Keep track of what configuration this was last
               updated under.  Setting to -1 means to delete
               the port when the current session is done. */

    struct port_info *new_config; /* If the port is reconfigged while
                     open, this will hold the new
                     configuration that should be
                     loaded when the current session
                     is done. */

    /* Data for the telnet processing */
    telnet_data_t tn_data;

    /* Is RFC 2217 mode enabled? */
    int is_2217;

    /* Holds whether break is on or not. */
    int break_set;

    /* Masks for RFC 2217 */
    unsigned char linestate_mask;
    unsigned char modemstate_mask;
    unsigned char last_modemstate;

    /* Read and write trace files, -1 if not used. */
    int wt_file;
    int rt_file;
    int bt_file;

    dev_info_t dinfo; /* device configuration information */
} port_info_t;
// ============================================================================



/* Create a port given the criteria. */
char * portconfig(char *portnum,
		  char *state,
		  char *timeout,
		  char *devname,
		  char *devcfg,
		  int  config_num);

/* Clear out any old ports on a reconfigure. */
void clear_old_port_config(int config_num);

/* Initialize the data transfer code. */
void dataxfer_init(void);

/* Show information about a port (or all ports if portspec is NULL).
   The parameters are all strings that the routine will convert to
   integers.  Error output will be generated on invalid data. */
void showports(struct controller_info *cntlr, char *portspec);

/* Show information about a port (as above) but in a one-line format. */
void showshortports(struct controller_info *cntlr, char *portspec);

/* Set the port's timeout.  The parameters are all strings that the
   routine will convert to integers.  Error output will be generated
   on invalid data. */
void setporttimeout(struct controller_info *cntlr,
		    char *portspec,
		    char *timeout);

/* Set the serial port's configuration.  The parameters are all
   strings that the routine will convert to integers.  Error output
   will be generated on invalid data. */
void setportdevcfg(struct controller_info *cntlr,
		   char *portspec,
		   char *devcfg);

/* Modify the DTR and RTS lines for the port. */
void setportcontrol(struct controller_info *cntlr,
		    char *portspec,
		    char *controls);

/* Set the enable state of a port (off, raw, telnet).  The parameters
   are all strings that the routine will convert to integers.  Error
   output will be generated on invalid data. */
void setportenable(struct controller_info *cntlr,
		   char *portspec,
		   char *enable);

/* Start data monitoring on the given port, type may be either "tcp" or
   "term" and only one direction may be monitored.  This return NULL if
   the monitor fails.  The monitor output will go to the controller
   via the controller_write() call. */
void *data_monitor_start(struct controller_info *cntlr,
			 char *type,
			 char *portspec);

/* Stop monitoring the given id. */
void data_monitor_stop(struct controller_info *cntlr,
		       void   *monitor_id);

/* Shut down the port, if it is connected. */
void disconnect_port(struct controller_info *cntlr,
		     char *portspec);

#endif /* DATAXFER */
